# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
#uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
from src.application import WiFiSTA
from src.application import WiFiAP

#import webrepl
#webrepl.start()
WiFiSTA.connect()
WiFiAP.startAp()
gc.collect()
