import os
import shutil
import subprocess

class Minify:
    def __init__(self):
        self.sourcedir = os.getcwd()
        
        self.ignoredFolders = ['build', 'dist', 'test', '.git', '__pycache__']
        self.ignoredFiles = ['min.py', '.gitignore', 'pymakr.conf', 'README.md', "indexori.html"]

        self.delete_all()
        self.minify()

    def get_all_py_files(self):
        py_files = []
        for root, directories, files in os.walk(self.sourcedir):
            directories[:] = [d for d in directories if d not in self.ignoredFolders]
            for filename in files:
                if filename.endswith('.py') and filename not in self.ignoredFiles:
                    filepath = os.path.join(root, filename)
                    py_files.append(filepath)
                elif filename not in self.ignoredFiles:
                    fileMisc = os.path.join(root, filename)
                    self.copy_file(fileMisc)
        return py_files

    def minify(self):
        files = self.get_all_py_files()
        for file in files:            
            proc = subprocess.Popen(
            ['pyminifier', file], stdout=subprocess.PIPE, encoding='utf-8')
            data, err = proc.communicate()
            self.create_file(file, data)

    def get_build_path(self, file):
        return os.path.join('build', os.path.relpath(file))
    
    def create_file(self, file, data):
        file = self.get_build_path(file)
        print("Create mode: " + file)
        if not os.path.exists(os.path.dirname(file)):
            os.makedirs(os.path.dirname(file))
        with open(file, 'w') as f:
            f.write(data)
    
    def copy_file(self, file):
        destFile = self.get_build_path(file)
        print("Copy mode: " + destFile)
        if not os.path.exists(os.path.dirname(destFile)):
            os.makedirs(os.path.dirname(destFile))
        shutil.copyfile(file, destFile)
    
    def delete_all(self):
        shutil.rmtree(os.path.join(os.getcwd(), 'build'), ignore_errors=True)

Minify()