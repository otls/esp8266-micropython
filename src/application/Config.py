from src.application.Data import Data
from src.config.config import config as appConfig

data = Data()

def config(key: str):
    if data.get(key):
        return data.get(key)
    if key in appConfig:
        return appConfig[key]
    return None