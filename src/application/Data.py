import json
import os

from src.application.Filesystem import dataDir

class Data:

    dataJson = {}
    filename = ''

    def __init__(self, filename: str = "data.json"):
        self.filename = filename
        fullpath = dataDir(self.filename)

        # check if file exists and create if not
        self.__checkFile()
        with open(fullpath, "r") as f:
            self.dataJson = json.load(f)

    # save data to file
    def save(self, key: str, value: str or int or float or bool):
        self.dataJson[key] = value
        self.__save()

    # save bulk data to file
    def saveBulk(self, data: dict):
        self.dataJson.update(data)
        self.__save()

    # Get data from data json
    def get(self, key: str):
        if key in self.dataJson:
            return self.dataJson[key]
        else:
            return None

    # Get all data from file
    def getAll(self):
        return self.dataJson

    # delete key from file
    def delete(self, key: str):
        if key in self.dataJson:
            del self.dataJson[key]
            self.__save()

    # delete bulk data from file
    def deleteBulk(self, data: tuple or list):
        for key in data:
            if key in self.dataJson:
                del self.dataJson[key]
        self.__save()

    # delete file
    def deleteFile(self):
        fullpath = dataDir(self.filename)
        os.remove(fullpath)

    # save data to file
    def __save(self):
        fullpath = dataDir(self.filename)
        with open(fullpath, "w") as f:
            json.dump(self.dataJson, f)

    # check if file exists
    def __checkFile(self):
        fullpath = dataDir(self.filename)
        try:
            f = open(fullpath, "r")
        except OSError:
            with open(fullpath, "w") as f:
                json.dump({}, f)
