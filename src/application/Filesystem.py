#get src directory
def srcDir(path: str) -> str:
    return '/src/' + path

# data directory
def dataDir(path: str) -> str:
    return srcDir('data/' + path)

def viewDir(path: str) -> str:
    return srcDir('views/' + path)
