import json

class Request:
    method = ''
    protocol = ''
    path = ''
    query = {}
    headers = {}
    body = None
    requestInput = {}

    def __init__(self, reqString: str):

        reqList =  reqString.split('\r\n\r\n')
        self.__reset()
        header = reqList[0]
        self.__extractHeader(header)

        # extract body / post data
        if len(reqList) > 1 and reqList[1] != '':
            self.__extractBody(reqList[1])

    def __extractBody(self, bodyString: str):
        contentType = self.getHeader('Content-Type')
        self.body = bodyString

        if contentType == 'application/json':
            data = json.loads(bodyString)
            self.requestInput.update(data)
        if contentType == 'application/x-www-form-urlencoded':
            urlDecoded = self.unquote(bodyString)
            # turn into dictionary
            bodyList = urlDecoded.split('&')
            for bodyItem in bodyList:
                bodyItemSplit = bodyItem.split('=')
                if len(bodyItemSplit) == 2:
                    self.requestInput[bodyItemSplit[0]] = bodyItemSplit[1]
    
    def __extractHeader(self, headerString: str):
        # check if not empty
        if headerString == '':
            return
        headerList = headerString.split('\r\n')
        #turn into dictionary
        for line in headerList:
            if line == '' or ':' not in line:
                continue
            key, value = line.split(': ')
            self.headers[key] = value
        self.__extractUri(headerList[0])
    
    def __extractUri(self, uriString: str):
        uriList = uriString.split(' ')
        path = uriList[1].split("?")
        query = {}
        if len(path) > 1:
            queryString = path[1]
            queryList = queryString.split('&')
            for queryItem in queryList:
                queryItemSplit = queryItem.split('=')
                if len(queryItemSplit) == 2:
                    query[queryItemSplit[0]] = queryItemSplit[1]

        self.method = uriList[0]
        self.protocol = uriList[2]
        self.path = path[0]
        self.query = query
        self.requestInput.update(query)

    def __reset(self):
        self.method = ''
        self.protocol = ''
        self.path = ''
        self.query = {}
        self.headers = {}
        self.body = None
        self.requestInput = {}

    def unquote(self, string, encoding='utf-8'):
        """unquote('abc%20def') -> 'abc def'.
        string may also be an UTF-8 encoded byte-string.
        """

        if not string:
            return ''

        if isinstance(string, str):
            string = string.encode('utf-8')

        bits = string.split(b'%')
        if len(bits) == 1:
            return string.decode(encoding)

        res = [bits[0]]
        append = res.append

        for item in bits[1:]:
            try:
                append(bytes([int(item[:2], 16)]))
                append(item[2:])
            except KeyError:
                append(b'%')
                append(item)

        return b''.join(res).decode(encoding)
    
    def getQuery(self, key: str):
        if key in self.query:
            return self.query[key]
        else:
            return ''
    
    def getHeader(self, key: str):
        if key in self.headers:
            return self.headers[key]
        else:
            return ''
    
    def getInput(self, key: str):
        if key in self.requestInput:
            return self.requestInput[key]
        else:
            return ''
    
    def dumpRequest(self):
        print("Method: {}".format(self.method))
        print("Protocol: {}".format(self.protocol))
        print("Path: {}".format(self.path))
        print("Query: {}".format(self.query))
        print("Headers: {}".format(self.headers))
        print("Body: {}".format(self.body))
        print("Request Input: {}".format(self.requestInput))
        print("")
        



