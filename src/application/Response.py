import json

from src.application.Filesystem import viewDir


class Response:
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'Content-Encoding': 'utf-8',
        'Server': 'Python/3.7.3',
        'Connection': 'close',
        'access-control-allow-origin': '*',
        'access-control-allow-headers': '*',
        'access-control-allow-methods': '*'
    }
    code = 200
    data = None
    protocol = 'HTTP/1.1'
    httpString = ""

    def __init__(self, code: int = 200, data: dict or str = None, headers: dict = {}):
        self.code = code
        self.data = data
        # merge headers
        self.headers.update(headers)
        # encode response to http

    def setHeaders(self, headers: dict):
        self.headers.update(headers)

    def setData(self, data: dict or str):
        self.data = data

    def setCode(self, code: int):
        self.code = code

    def encoded(self, encoding: str = 'utf-8') -> str:
        self.__encodeResponseToHttp()
        return self.httpString.encode(encoding)

    def dump(self):
        print("\nResponse:")
        print(self.httpString.encode('utf-8'))
        print("\n")

    def __encodeResponseToHttp(self):
        httpString = self.protocol + ' ' + str(self.code) + '\n'
        httpString += self.__getHeaders() + '\n'
        httpString += self.__getData()

        self.httpString = httpString
    
    def __getHeaders(self) -> str:
        headers = ''
        for key in self.headers:
            headers += key + ': ' + self.headers[key] + '\n'
        return headers
    
    def __getData(self) -> str:
        data = ''
        if self.data is not None:
            if isinstance(self.data, dict):
                data = json.dumps(self.data)
            else:
                data = self.data
        return data
