import socket
from src.application.Filesystem import viewDir
from src.application.Response import Response
from src.application.blink import blink_coming

from src.application.Config import config
from src.config import route
from src.application.Request import Request

class Server:
    def __init__(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((config('host'), config('port')))

        print("init server")

    def start(self):
        print("Server started..\n")
        self.__listen()
        self.__accept()
    
    def __listen(self):
        self.server.listen(config('listen_max'))

    def __accept(self):

        while True:
            # Wait for client connections
            client, client_address = self.server.accept()
            # print client_address
            blink_coming()
            print("\nREQUEST")
            print("Accepted connection from {}".format(client_address))
            # Get the client request
            reqString = client.recv(config('recv_max')).decode("utf-8", "ignore")
            #parse the request
            req = Request(reqString)
            # assign the client to the self.client
            self.client = client
            #route the request
            self.__routing(req)
            self.client.close()
    
    def __routing(self, request: Request):
        # check if the path exist
        self.__checkpath(request)
        # check if the method is correct
        self.__checkMethod(request)
        # run the controller
        self.__runController(request)

    def __checkpath(self, request: Request):
        if not request.path in route.route:
            return self.send404()

    def __checkMethod(self, request: Request):
        try:
            reqPath =request.path
            reqMethod = request.method

            if reqPath in route.route and reqMethod in route.route[reqPath]['method']:
                if request.method != route.route[request.path]['method']:
                    return self.send405()
        except:
            return self.send405()
    
    def __runController(self, request: Request):
        reqPath = request.path
        try:
            if route.route[reqPath]['action'](request, self):
                route.route[reqPath]['action'](request, self)
        except Exception as e:
            print(e)
            return self.send500({'msg': e})

    def sendView(self, view: str):
        view = viewDir(view)
        data = open(view, 'r').read()
        return self.sendResponse(200, data, {'Content-Type': 'text/html; charset=utf-8'})
    
    def send404(self, data: dict or str = {'msg': '404 Not Found'}):
        self.sendResponse(404, data)

    def send405(self, data: dict or str = {'msg': '405 Method Not Allowed'}):
        self.sendResponse(405, data)
    
    def send400(self, data: dict or str = {'msg': '400 Bad Request'}):
        self.sendResponse(400, data)
    
    def send500(self, data: dict or str = {'msg': '500 Internal Server Error'}):
        self.sendResponse(500, data)
    
    def send200(self, data: dict or str = {'msg': 'OK'}):
        self.sendResponse(200, data)

    def send201(self, data: dict or str = {'msg': 'Created'}):
        self.sendResponse(201, data)

    def send204(self, data: dict or str = {'msg': 'No Content'}):
        self.sendResponse(204, data)

    def sendResponse(self, code: int = 200, data: dict or str = None, headers: dict = {}):
        response = Response(code, data, headers)
        encodedResponse = response.encoded()
        self.client.sendall(encodedResponse)
