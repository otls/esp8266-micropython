import network
from src.application.Config import config

def startAp():
    ssid = config('wifi_ap_ssid')
    password = config('wifi_ap_password')

    #create esp8266 access point
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    ap.config(essid=ssid, password=password)
    print('network config:', ap.ifconfig())
    
    
    
    print('AP started')
    print('SSID: ' + ssid)
    print('Password: ' + password)
    
