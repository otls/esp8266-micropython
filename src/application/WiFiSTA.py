import network
from src.application.blink import blink_idle, blink_process_on
from src.application.Config import config

def connect():

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)

    ssid = config('wifi_ssid')
    password = config('wifi_password')

    if not wlan.isconnected():
        print('\nconnecting to network...')
        print("ssid: " + ssid)
        print("password: " + password)
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            blink_process_on()
    
    blink_idle()

    print('network config:', wlan.ifconfig())
