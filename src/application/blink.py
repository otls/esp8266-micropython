from machine import Pin
from src.config.config import config
import time

pin = Pin(config['blink_pin'], Pin.OUT)

def blink_process_on():
    pin.on()
    time.sleep(0.2)
    pin.off()
    time.sleep(0.2)

def blink_idle():
    pin.off()

def blink_coming():
    pin.on()
    time.sleep(0.1)
    pin.off()
    time.sleep(0.1)

    pin.on()
    time.sleep(0.1)
    pin.off()
    time.sleep(0.1)
