from src.controllers.IndexController import IndexController
from src.controllers.RelaySwitchFanController import RelaySwitchFanController
from src.controllers.SettingController import SettingController

index = IndexController()
fanController = RelaySwitchFanController()
settingController = SettingController()

route = {
    '/': {
        'method': 'GET',
        'action': index.index,
    },
    '/fan': {
        'method': 'GET',
        'action': fanController.switchSpeed,
    },
    '/fan/off': {
        'method': 'GET',
        'action': fanController.speedOff,
    },
    '/fanoscilate': {
        'method': 'GET',
        'action': fanController.oscilate,
    },
    '/apifconfig': {
        'method': 'POST',
        'action': settingController.wifiApSetting
    }
}