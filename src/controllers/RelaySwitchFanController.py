from src.lib.typing.RelaySwitch import RelaySwitch
from src.lib.typing.ServoOscilation import ServoOscilation
from src.lib.RelaySwitchFan import RelaySwitchFan

class RelaySwitchFanController:
    
    def __init__(self):
        #speeds definition
        low = RelaySwitch(5, 'low')
        medium = RelaySwitch(4, 'medium')
        high = RelaySwitch(12, 'high')
        #oscilation definition
        oscilation = ServoOscilation(pin=14, onAngle=20, offAngle=120, freq=50)
        #fan switch definition
        self.switch = RelaySwitchFan(oscilation, low, medium, high)
    
    def switchSpeed(self, req, res):
        speed = req.getQuery('speed')
        if speed in self.switch.speeds:
            self.switch.switchSpeed(speed)
            res.send200({'msg': 'Speed switched to ' + speed})
        elif speed == 'off':
            self.switch.speedAllOff()
            res.send200({'msg': 'Fan Idle'})
        else:
            res.send400({'msg': 'Invalid speed'})
    
    def speedOff(self, req, res):
        self.switch.speedAllOff()
        res.send200({'msg': 'All speeds off'})
    
    def oscilate(self, req, res):
        state = req.getQuery('state')
        if state == 'on':
            # upsidedown logic
            self.switch.oscilate(False)
            res.send200({'msg': 'Oscilation on'})
        elif state == 'off':
            # upsidedown logic
            self.switch.oscilate(True)
            res.send200({'msg': 'Oscilation off'})
        else:
            res.send400({'msg': 'Invalid state'})