import network
from src.application.Data import Data

class SettingController:
    def __init__(self):
        self.data = Data()

    def wifiClientSetting(self, req, res):
        ssid = req.getQuery('ssid')
        password = req.getQuery('password')

        if (ssid is not '' and password is not '') and (isinstance(ssid, str) and isinstance(password, str)):
            setting = {
                'wifi_ssid': ssid,
                'wifi_password': password
            }
            try:
                self.data.saveBulk(setting)
                res.send200()
            except Exception as e:
                res.send500("save setting error: " + str(e))
        else:
            res.send400("ssid or password is empty")

    def wifiApSetting(self, req, res):
        ssid = req.getQuery('ssid')
        password = req.getQuery('password')

        if (ssid is not '' and password is not '') and (isinstance(ssid, str) and isinstance(password, str)):
            setting = {
                'wifi_ap_ssid': ssid,
                'wifi_ap_password': password
            }
            try:
                self.data.saveBulk(setting)
                res.send200()
            except Exception as e:
                res.send500("save setting error: " + str(e))
        else:
            res.send400("ssid or password is empty")

    def getApIf(self, req, res):
        ap = network.WLAN(network.AP_IF)
        res.send200(ap.ifconfig())