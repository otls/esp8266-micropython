from src.lib.typing.RelaySwitch import RelaySwitch
from src.lib.typing.ServoOscilation import ServoOscilation

class RelaySwitchFan:
    speeds = {}

    def __init__(self, osilation: ServoOscilation, *speeds: RelaySwitch):
        self.oscilation = osilation
        for speed in speeds:
            self.speeds[speed.name] = speed

        self.speedAllOff()
        self.oscilate(False)


    def switchSpeed(self, speedName: str):
        if speedName in self.speeds:
            self.speedAllOff()
            #upside down logic
            self.speeds[speedName].switch.off()
            self.speeds[speedName].state = True

    def speedAllOff(self):
        for speed in self.speeds:
            #upside down logic
            self.speeds[speed].switch.on()
            self.speeds[speed].state = False

    def oscilate(self, state: bool):
        if state:
            self.oscilation.servo.duty(self.oscilation.onAngle)
        else:
            self.oscilation.servo.duty(self.oscilation.offAngle)