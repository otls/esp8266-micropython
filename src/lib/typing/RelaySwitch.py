from machine import Pin

class RelaySwitch:
    def __init__(self, pin, name):
        self.pin = pin
        self.name = name
        self.state = False
        self.switch = Pin(pin, Pin.OUT)