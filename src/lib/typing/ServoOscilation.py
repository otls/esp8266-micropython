from machine import Pin, PWM

class ServoOscilation:
    def __init__(self, pin: int, onAngle: float, offAngle: float, freq: int = 50) -> None:
        self.pin = pin
        self.onAngle = onAngle
        self.offAngle = offAngle
        self.servo = PWM(Pin(pin, Pin.OUT), freq=freq)